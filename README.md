1. User Login

2. Product Search page

3. Search Result page shows At least 20 different type of plans for each type of insurance like

   - Car Insurance
   - Bike Insurance
   - Property Insurance
   - Term Plan

4. Product card selected from above search result shows below required information

   - Company logo
   - Current Age
   - Gender
   - Current illness if any (Diabetic, BP, etc...)
   - Smoking habits (Occasional, Regular, No Smoker)
   - Estimated Insurance Amount
   - Add to Cart button

5. Cart menu will provide option to verify the selected details along with removal of selected item.
6. User will be able to move cart items to checkout page.
7. Order page shows below information

   - Product title
   - Number of items selected
   - Price of selected policy
   - GST amount
   - Offer code
   - Total bill
