import { InsuranceHeader } from "../../components/InsuranceHeader/InsuranceHeader"
import SearchBox from "../../components/SearchBox"
import { useEffect } from "react"
import { useSelector } from "react-redux"
import { Link, useHistory } from "react-router-dom"
import "./index.css"
import { IC_BIKE, IC_CAR, IC_PROPERTY, IC_TERM } from "../../utils/images"
import Header from "../../components/Header"

const Home = () => {
  const { isLoggedIn } = useSelector((store) => store.auth)
  const history = useHistory()
  useEffect(() => (!isLoggedIn ? history.push("/login") : null), [isLoggedIn])

  return (
    <>
      <Header />

      <main>
        <section>
          <div className="container p-5">
            <SearchBox />

            <InsuranceHeader />
            <div className="row">
              <div className="col col-lg-3 col-md-4 col-sm-6 mb-3">
                <Link to="/insurances/bike/1">
                  <div className="card bike-card">
                    <div className="avatar mx-auto rounded-circle">
                      <img src={IC_BIKE} className="img-responsive" alt="bike" />
                    </div>

                    <div className="card-body">
                      <h4 className="card-title text-center">Bike Insurance</h4>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col col-lg-3 col-md-4 col-sm-6 mb-3">
                <Link to="/insurances/car/1">
                  <div className="card car-card">
                    <div className="avatar mx-auto rounded-circle">
                      <img src={IC_CAR} className="img-responsive" alt="car" />
                    </div>

                    <div className="card-body">
                      <h4 className="card-title text-center">Car Insurance</h4>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col col-lg-3 col-md-4 col-sm-6 mb-3">
                <Link to="/insurances/property/1">
                  <div className="card prop-card">
                    <div className="avatar mx-auto rounded-circle">
                      <img src={IC_PROPERTY} className="img-responsive" alt="property" />
                    </div>

                    <div className="card-body">
                      <h4 className="card-title text-center">Property Insurance</h4>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col col-lg-3 col-md-4 col-sm-6 mb-3">
                <Link to="/insurances/term/1">
                  <div className="card term-card">
                    <div className="avatar mx-auto rounded-circle">
                      <img src={IC_TERM} className="img-responsive" alt="term" />
                    </div>

                    <div className="card-body">
                      <h4 className="card-title text-center">Term Insurance</h4>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  )
}
export default Home
