import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import VoucharCode from "../../components/VoucharCode"
import { ActionTypes } from "../../store/actions/actionTypes"
import { getRandomBgColor } from "../../utils/utils"
import { CartItemBody } from "./CartItemBody"
import "./index.css"

const Cart = () => {
  const { cartItems, totalPrice, selectedItems } = useSelector((store) => store.cart)
  const [code, setCode] = useState({ code: "", isApplied: false })
  const [gst, setGst] = useState(0)
  const [discount, setDiscount] = useState(0)
  const [finalPrice, setFinalPrice] = useState(0)
  const dispatch = useDispatch()

  const handleDelete = (index, eleId) => {
    dispatch({ type: ActionTypes.DELETE_ITEM_FROM_CART, payload: index })
    // const element = document.getElementById("cart-item-" + eleId)
    // if (element) {
    //   element.style.transform = "translateY(-100px)"
    //   element.style.opacity = "0"
    // }
    // setTimeout(() => {
    //   dispatch({ type: ActionTypes.DELETE_ITEM_FROM_CART, payload: index })
    // }, 800)
  }

  useEffect(() => setGst(Math.floor(totalPrice * 0.12)), [totalPrice])
  useEffect(() => setFinalPrice(Math.floor(totalPrice + gst - discount)), [gst, discount])
  useEffect(() => (code?.isApplied ? setDiscount(Math.floor(totalPrice * 0.05)) : setDiscount(0)), [code])

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            PolicyMarket
          </Link>
        </div>
      </nav>

      <main>
        <section className="container pt-5">
          <div className="row cart-wrapper">
            <h1 className="display-3">
              <i className="fa fa-shopping-cart"></i>Cart
            </h1>
            {cartItems && cartItems.length > 0 ? (
              <>
                <div className="col-lg-9 col-md-12">
                  <div className="container">
                    <div className="accordion" id="cart-list">
                      <div className="card my-3 border border-secondary rounded-3">
                        <div className="d-flex flex-row align-items-center m-3">
                          <div
                            onClick={() =>
                              selectedItems === cartItems.length
                                ? dispatch({ type: ActionTypes.CART_DESELECT_ALL })
                                : dispatch({ type: ActionTypes.CART_SELECT_ALL })
                            }
                            role="button"
                            className={"btn-cart-select-all " + (selectedItems === cartItems.length ? "selected" : "")}>
                            <i className="fa fa-check fa-lg ms-2"></i>
                            <b className="text-secondary me-3">Select All</b>
                          </div>
                          <span className="mx-3">{selectedItems + " out of " + cartItems.length + " selected"}</span>
                          <div>
                            {selectedItems > 0 && (
                              <button
                                className="btn btn-outline-primary btn-rounded"
                                onClick={() => dispatch({ type: ActionTypes.DELETE_SELECTED })}>
                                Delete Selected
                              </button>
                            )}
                          </div>
                        </div>
                      </div>
                      {cartItems &&
                        cartItems.map((item, itemIndex) => (
                          <div
                            key={"cart-item-" + item?.dateAdded}
                            id={"-cart-item-" + item?.dateAdded}
                            className="accordion-item bg-white">
                            <div className="accordion-header position-relative">
                              <div
                                onClick={() =>
                                  dispatch({ type: ActionTypes.CHANGE_CART_SELECTION, payload: itemIndex })
                                }
                                role="button"
                                className={"btn-cart-select position-absolute" + (item?.selected ? " selected" : "")}>
                                <i className="fa fa-check fa-sm"></i>
                              </div>
                              <div
                                onClick={() => handleDelete(itemIndex, item.dateAdded)}
                                role="button"
                                className="btn-cart-delete position-absolute">
                                <i className="fa fa-trash"></i> Delete
                              </div>
                              <button
                                className="d-flex dlex-row align-items-center"
                                class="accordion-button collapsed"
                                type="button"
                                data-mdb-toggle="collapse"
                                aria-expanded="false"
                                data-mdb-target={"#collapse-" + itemIndex}
                                aria-controls={"collapse-" + itemIndex}>
                                <div className="cart-item-avatar me-2">
                                  <img
                                    src={item.companyLogo}
                                    style={{ backgroundColor: getRandomBgColor(itemIndex) }}
                                    className={"img-responsive "}
                                    alt="bike"
                                  />
                                </div>

                                <div className="col text-start">
                                  <h6 className="card-title">{item.companyName + " " + item.name}</h6>
                                  <p className="card-price mb-0">
                                    <span className="price-tag-0 me-2">{"Rs " + item?.userData?.price}</span>
                                    <span className="price-tag-2">{`${item?.discount}% discount`}</span>
                                  </p>
                                </div>
                              </button>

                              <div
                                className="accordion-collapse collapse"
                                id={"collapse-" + itemIndex}
                                aria-labelledby={"cart-item-" + itemIndex}>
                                <div className="accordion-body">
                                  <CartItemBody item={item} />
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12 my-5 mx-auto summray-wrapper">
                  {totalPrice > 0 && (
                    <div className="card shadow-3 rounded-3 p-3 h-auto">
                      <h4 className="card-title text-center">Summary</h4>
                      <hr className="my-1 text-primary" />
                      <VoucharCode code={code} setCode={setCode} />
                      <table className="table">
                        <tbody>
                          <tr className="text-muted font-weight-bold">
                            <td>Total Items</td>
                            <td>{selectedItems}</td>
                          </tr>
                          <tr className="text-muted font-weight-bold">
                            <td>Amount</td>
                            <td>{totalPrice}Rs</td>
                          </tr>
                          <tr className="text-warning font-weight-bold">
                            <td>GST (5%)</td>
                            <td>+{gst} Rs</td>
                          </tr>
                          {code?.isApplied && (
                            <>
                              <tr className="text-muted font-weight-bold">
                                <td>Subtotal</td>
                                <td>{totalPrice + gst} Rs</td>
                              </tr>
                              <tr className="font-weight-bold">
                                <td>Coupen Code</td>
                                <td>{code?.code}</td>
                              </tr>
                              <tr className="font-weight-bold">
                                <td>Coupen Discount</td>
                                <td>-{discount} Rs</td>
                              </tr>
                            </>
                          )}

                          <tr className="text-success font-weight-bold">
                            <td>Toal Amount</td>
                            <td>{finalPrice} Rs</td>
                          </tr>
                        </tbody>
                      </table>
                      <Link
                        to={{
                          pathname: "/checkout",
                          state: {
                            data: cartItems.filter((i) => i.selected),
                            code,
                            finalPrice,
                            totalPrice,
                            gst,
                            discount,
                          },
                        }}
                        className="btn btn-primary btn-rounded btn-lg">
                        Checkout
                      </Link>
                    </div>
                  )}
                </div>
              </>
            ) : (
              <>
                <div className="col-md-9 col-sm-12">
                  <div className="alert alert-primary m-5 border border-primary rounded-3">
                    <h4 className="alert-heading">Your cart is empty</h4>
                    <h6>
                      <i className="fa fa-cart-plus"></i> Add items to your cart
                    </h6>
                    <Link className="btn btn-primary btn-lg btn-rounded" to="/">
                      Browse Insurances
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>
        </section>
      </main>
    </>
  )
}

export default Cart
