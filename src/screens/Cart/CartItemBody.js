export const CartItemBody = ({ item }) => {
  const userData = item?.userData
  return (
    <>
      {item.type === "CAR" || item.type === "BIKE" ? (
        <table className="table">
          <tbody>
            <tr>
              <td>Person name</td>
              <td>{userData.name}</td>
            </tr>
            <tr>
              <td>Age</td>
              <td>{userData.age + " Years"}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td className="text-primary">{userData.email ?? "- -"}</td>
            </tr>
            <tr>
              <td>Mobile No</td>
              <td className="text-primary">{"+91 " + userData?.mobile ?? "- -"}</td>
            </tr>
            <tr>
              <td>Vehical No</td>
              <td>{userData.vehicalNo}</td>
            </tr>
            <tr>
              <td>Vehical Modal</td>
              <td>{userData.vehicalModal}</td>
            </tr>
            <tr>
              <td>Registraion Year</td>
              <td>{userData.registrationYear}</td>
            </tr>
            <tr>
              <td>Plan Duration</td>
              <td>{userData.plan}</td>
            </tr>
            <tr>
              <td>Estimated Insurred Value</td>
              <td>{userData.idv}</td>
            </tr>
            <tr>
              <td>Insurance Amount</td>
              <td>{userData.mrp}</td>
            </tr>
            <tr>
              <td>Discount</td>
              <td>{item.discount}</td>
            </tr>
            <tr className="table-primary">
              <td>Final Payment</td>
              <td>{userData.price}</td>
            </tr>
          </tbody>
        </table>
      ) : item.type === "PROPERTY" ? (
        <table className="table">
          <tbody>
            <tr>
              <td>Person name</td>
              <td>{userData.name}</td>
            </tr>
            <tr>
              <td>Age</td>
              <td>{userData.age + " Years"}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{userData.email}</td>
            </tr>
            <tr>
              <td>Mobile No</td>
              <td>{"+91 " + userData.phone}</td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>{userData.gender}</td>
            </tr>
            <tr>
              <td>Property Type</td>
              <td>{userData.propertyType}</td>
            </tr>
            <tr>
              <td>How old property is?</td>
              <td>{userData.propertyAge}</td>
            </tr>
            <tr>
              <td>Market Value</td>
              <td>{userData.marketValue}</td>
            </tr>
            <tr>
              <td>Plan</td>
              <td>{userData.plan}</td>
            </tr>
            <tr>
              <td>Estimated Insurred Value</td>
              <td>{userData.idv}</td>
            </tr>
            <tr>
              <td>Insurance Amount</td>
              <td>{userData.mrp}</td>
            </tr>
            <tr>
              <td>Discount</td>
              <td>{item.discount}</td>
            </tr>
            <tr className="table-primary">
              <td>Final Payment</td>
              <td>{userData.price}</td>
            </tr>
          </tbody>
        </table>
      ) : item.type === "TERM" ? (
        <table className="table">
          <tbody>
            <tr>
              <td>Person name</td>
              <td>{userData.name}</td>
            </tr>
            <tr>
              <td>Age</td>
              <td>{userData.age + " Years"}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{userData.email}</td>
            </tr>
            <tr>
              <td>Mobile No</td>
              <td>{"+91 " + userData.phone}</td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>{userData.gender}</td>
            </tr>
            <tr>
              <td>Salary Range</td>
              <td>{userData.salaryRange}</td>
            </tr>
            <tr>
              <td>Smoking Habbit</td>
              <td>{userData.smokingHabbit}</td>
            </tr>
            <tr>
              <td>Illness</td>
              <td>{userData.illness}</td>
            </tr>
            <tr>
              <td>Plan Type</td>
              <td>{userData.termPlan}</td>
            </tr>
            <tr>
              <td>Plan Duration</td>
              <td>{userData.plan}</td>
            </tr>
            <tr>
              <td>Estimated Insurred Value</td>
              <td>{userData.idv}</td>
            </tr>
            <tr className="table-primary">
              <td>Insurance Payment</td>
              <td>{userData.price}</td>
            </tr>
          </tbody>
        </table>
      ) : null}
    </>
  )
}
