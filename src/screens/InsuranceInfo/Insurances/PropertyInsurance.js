import { ErrorMessage, Field, Form, Formik } from "formik"
import { getPriceTags } from "../../../utils/utils"
import * as Yup from "yup"
import { ActionTypes } from "../../../store/actions/actionTypes"
import { useDispatch } from "react-redux"
import InsuranceMetaData from "../../../components/InsuranceMetaData"
import { toast } from "react-toastify"

export const PropertyInsurance = ({ insurance }) => {
  const dispatch = useDispatch()
  return (
    <div className="insurance-info">
      <div className="row align-items-center">
        <div className="col-md-3 col-sm-12 company-logo my-3">
          <img src={insurance.companyLogo} alt="bike" />
        </div>

        <div className="col-md-9 col-sm-12">
          <h3 className="card-title">{insurance.companyName + " " + insurance.name}</h3>
          <h4 className="mb-0">
            {getPriceTags(insurance.annualPlan, insurance.type, insurance.discount).map((tag, tagId) => (
              <span key={`price-tag-${insurance._id}-${tagId}`} className={`price-tag-${tagId} me-2`}>
                {tag}
              </span>
            ))}
          </h4>
        </div>
      </div>
      <InsuranceMetaData />

      <div className="container my-5">
        <Formik
          initialValues={{
            name: "",
            age: "",
            mobile: "",
            email: "",
            gender: "",
            propertyType: "",
            propertyAge: "",
            marketValue: "",
            plan: "",
          }}
          validationSchema={Yup.object().shape({
            name: Yup.string().required("Required"),
            age: Yup.number().required("Required").min(18, "Can't be less than 18").max(99, "Can't be more than 99"),
            mobile: Yup.string()
              .required("Required")
              .matches(/^[0-9]{10,10}$/, "Invalid mobile number"),
            email: Yup.string().email("Invalid email id"),
            gender: Yup.string().required("Required"),
            propertyType: Yup.string().required("Required"),
            propertyAge: Yup.string().required("Required"),
            plan: Yup.string().required("Required"),
            marketValue: Yup.number()
              .min(50000, "Property values below Rs.50,000 are not applicable.")
              .required("Required"),
          })}
          onSubmit={(values) => {
            const mrp = insurance?.annualPlan?.[values.plan]
            const price = mrp - mrp * (insurance.discount / 100)
            const idv = values.marketValue
            let data = { ...insurance, userData: { ...values, price, mrp, idv } }
            dispatch({ type: ActionTypes.ADD_ITEM_TO_CART, payload: data })
            toast.success("Successfully added to your cart", {
              position: "top-right",
              autoClose: 3000,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
            })
          }}>
          <Form className="row justify-content-center">
            <div className="col-lg-6 col-md-9 col-sm-12 shadow-5 bg-light rounded-3 p-3">
              <h1 className="display-6 form-title">Fill Details</h1>
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Name</label>
                    <Field className="form-control" placeholder="Name" name="name" />
                    <ErrorMessage name="name" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Your Age</label>
                    <Field type="number" className="form-control" placeholder="Age (18 -99)" name="age" />
                    <ErrorMessage name="age" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Phone</label>
                    <div className="input-group">
                      <span className="input-group-text" id="addon-wrapping">
                        +91
                      </span>
                      <Field className="form-control" placeholder="XXXXXXXXXX" name="mobile" />
                    </div>
                    <ErrorMessage name="mobile" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Email</label>
                    <Field className="form-control" placeholder="asdfgh@example.com" name="email" />
                    <ErrorMessage name="email" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Gender</label>
                    <div>
                      <div className="form-check form-check-inline">
                        <Field className="form-check-input" type="radio" name="gender" value="MALE" />
                        <label className="form-check-label">Male</label>
                      </div>
                      <div className="form-check form-check-inline">
                        <Field className="form-check-input" type="radio" name="gender" value="FEMALE" />
                        <label className="form-check-label">Feale</label>
                      </div>
                      <div className="form-check form-check-inline">
                        <Field className="form-check-input" type="radio" name="gender" value="OTHER" />
                        <label className="form-check-label">Other</label>
                      </div>
                    </div>
                    <ErrorMessage name="gender" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Property Type</label>
                    <div className="position-relative">
                      <Field className="form-control select-input" as="select" name="propertyType">
                        <option value="" disabled>
                          Select
                        </option>
                        <option value="Tenament House">Tenament House</option>
                        <option value="Apartment House">Apartment House</option>
                        <option value="Other">Other</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                    <ErrorMessage name="propertyType" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-12">
                  <div className="my-2">
                    <label className="text-label">Property Age (How old it is?)</label>
                    <div className="position-relative">
                      <Field className="form-control" as="select" name="propertyAge">
                        <option value="" disabled>
                          Select
                        </option>
                        <option value="0-25 years">0-25 years</option>
                        <option value="25-40 years">25-40 Years</option>
                        <option value="40-50 years">40-50 Years</option>
                        <option value="more than 50 years">More than 50 Years</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                    <ErrorMessage name="propertyAge" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-12">
                  <div className="my-2">
                    <label className="text-label">Market Vlaue of Property</label>
                    <div className="input-group">
                      <span className="input-group-text" id="addon-wrapping">
                        Rs.
                      </span>
                      <Field className="form-control" placeholder="XXXXXXX" name="marketValue" type="number" />
                    </div>
                    <ErrorMessage name="marketValue" component="p" className="text-danger error-text" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="my-2">
                    <label className="text-label">Insurance Plan</label>
                    <div className="position-relative">
                      <Field name="plan" as="select" className="form-control">
                        <option value="" disabled>
                          Select
                        </option>
                        <option value={"structure"}>Structure</option>
                        <option value={"content"}>Content</option>
                        <option value={"both"}>Both</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                  </div>
                </div>
                <div className="col-12">
                  <Field name="plan">
                    {({ field }) => <h3 className="note note-primary"> ₹ {insurance?.annualPlan?.[field.value]}</h3>}
                  </Field>
                  <Field name="marketValue">
                    {({ field, meta }) =>
                      meta.touched && !meta.error ? (
                        <>
                          <h5 className="note note-secondary">{"Insurred Value**: ₹ " + field.value ?? "- -"}</h5>
                          <small className="text-muted">**This is estimated insurred direct value.</small>
                          <p className="text-warning">Based on our inspection value can be decreased.</p>
                        </>
                      ) : null
                    }
                  </Field>
                </div>
              </div>
              <button className="btn btn-primary btn-rounded btn-lg" type="submit">
                Add to Cart
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  )
}
