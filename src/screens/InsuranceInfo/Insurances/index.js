import { CarInsurance } from "./CarInsurance"
import { PropertyInsurance } from "./PropertyInsurance"
import { TermInsurance } from "./TermInsurance"

export const getInsuranceView = (insurance) => {
  switch (insurance.type) {
    case "BIKE":
      return <CarInsurance insurance={insurance} />
    case "CAR":
      return <CarInsurance insurance={insurance} />
    case "PROPERTY":
      return <PropertyInsurance insurance={insurance} />
    case "TERM":
      return <TermInsurance insurance={insurance} />
    default:
      return null
  }
}
