import { ErrorMessage, Field, Form, Formik } from "formik"
import { getPriceTags } from "../../../utils/utils"
import * as Yup from "yup"
import { useDispatch } from "react-redux"
import { ActionTypes } from "../../../store/actions/actionTypes"
import InsuranceMetaData from "../../../components/InsuranceMetaData"
import { toast } from "react-toastify"

export const CarInsurance = ({ insurance }) => {
  const dispatch = useDispatch()
  return (
    <div className="insurance-info">
      <div className="row align-items-center">
        <div className="col-md-3 col-sm-12 company-logo my-3">
          <img src={insurance.companyLogo} alt="bike" />
        </div>

        <div className="col-md-9 col-sm-12">
          <h3 className="card-title">{insurance.companyName + " " + insurance.name}</h3>
          <h4 className="mb-0">
            {getPriceTags(insurance.annualPlan, insurance.type, insurance.discount).map((tag, tagId) => (
              <span key={`price-tag-${insurance._id}-${tagId}`} className={`price-tag-${tagId} me-2`}>
                {tag}
              </span>
            ))}
          </h4>
        </div>
      </div>
      <InsuranceMetaData />

      <div className="container my-5">
        <Formik
          initialValues={{
            name: "",
            age: 18,
            mobile: "",
            email: "",
            vehicalNo: "",
            vehicalModal: "",
            registrationYear: new Date().getFullYear(),
            plan: 1,
          }}
          validationSchema={Yup.object().shape({
            name: Yup.string().required("Required"),
            age: Yup.number().required("Required").min(18, "Can't be less than 18").max(99, "Can't be more than 99"),
            mobile: Yup.string()
              .required("Required")
              .matches(/^[0-9]{10,10}$/, "Invalid mobile number"),
            email: Yup.string().email("Invalid email id"),
            vehicalNo: Yup.string()
              .required("Required")
              .matches(/^[A-Za-z]{2,2}[0-9]{2,2}[A-Za-z]{2,2}[0-9]{4,4}$/, "Enter valid vehical no"),
            vehicalModal: Yup.string().required("Required"),
            registrationYear: Yup.number().min(1950).max(new Date().getFullYear()).required("Required"),
            plan: Yup.number().required("Required"),
          })}
          onSubmit={(values) => {
            const mrp = values.plan * insurance.annualPlan
            const price = Math.floor(mrp - mrp * (insurance.discount / 100))
            const data = { ...insurance, userData: { ...values, price, idv: insurance.idv } }
            dispatch({ type: ActionTypes.ADD_ITEM_TO_CART, payload: data })
            toast.success("Successfully added to your cart", {
              position: "top-right",
              autoClose: 3000,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
            })
          }}>
          <Form className="row justify-content-center">
            <div className="col-lg-6 col-md-9 col-sm-12 shadow-5 bg-light rounded-3 p-3">
              <h1 className="display-6 form-title">Fill Details</h1>
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Name</label>
                    <Field className="form-control" placeholder="Name" name="name" />
                    <ErrorMessage name="name" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Your Age</label>
                    <Field type="number" className="form-control" placeholder="Age (18 -99)" name="age" />
                    <ErrorMessage name="age" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Phone</label>
                    <div className="input-group">
                      <span className="input-group-text" id="addon-wrapping">
                        +91
                      </span>
                      <Field className="form-control" placeholder="XXXXXXXXXX" name="mobile" />
                    </div>
                    <ErrorMessage name="mobile" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Email</label>
                    <Field className="form-control" placeholder="asdfgh@example.com" name="email" />
                    <ErrorMessage name="email" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Vehical No</label>
                    <Field className="form-control" placeholder="XX01AA0101" name="vehicalNo" />
                    <ErrorMessage name="vehicalNo" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Vehical Model</label>
                    <Field className="form-control" placeholder="Model" name="vehicalModal" />
                    <ErrorMessage name="vehicalModal" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Registraion Year</label>
                    <Field type="number" className="form-control" placeholder="Model" name="registrationYear" />
                    <ErrorMessage name="registrationYear" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Insurance Plan</label>
                    <Field name="plan" as="select" className="form-control">
                      <option value={1}>1 Year</option>
                      <option value={3}>3 Years</option>
                      <option value={5}>5 Years</option>
                    </Field>
                  </div>
                </div>
              </div>
              <div className="row mb-4 mt-3">
                <div className="col-12 align-self-end">
                  <Field name="plan">
                    {({ field }) => (
                      <h3 className="note note-primary">
                        {"Insurance Price: " +
                          getPriceTags(field.value * insurance.annualPlan, insurance.type, insurance.discount)[0]}
                      </h3>
                    )}
                  </Field>
                  <h5 className="note note-secondary">{"Insurred Value**: ₹ " + insurance?.idv ?? "- -"}</h5>
                  <small className="text-muted">**This is estimated insurred direct value.</small>
                </div>
              </div>
              <button className="btn btn-primary btn-rounded btn-lg" type="submit">
                Add to Cart
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  )
}
