import { ErrorMessage, Field, Form, Formik } from "formik"
import { getPriceTags } from "../../../utils/utils"
import * as Yup from "yup"
import { ActionTypes } from "../../../store/actions/actionTypes"
import { useDispatch } from "react-redux"
import InsuranceMetaData from "../../../components/InsuranceMetaData"
import { toast } from "react-toastify"
import { SalRange, SmokingHabbits, TermPlanYears } from "../../../utils/const"

export const TermInsurance = ({ insurance }) => {
  const dispatch = useDispatch()
  return (
    <div className="insurance-info">
      <div className="row align-items-center">
        <div className="col-md-3 col-sm-12 company-logo my-3">
          <img src={insurance.companyLogo} alt="bike" />
        </div>

        <div className="col-md-9 col-sm-12">
          <h3 className="card-title">{insurance.companyName + " " + insurance.name}</h3>
          <h4 className="mb-0">
            <span className={"price-tag-2 me-2"}>
              {getPriceTags(insurance.annualPlan, insurance.type, insurance.discount)[2]}
            </span>
          </h4>
        </div>
      </div>
      <InsuranceMetaData />

      <div className="container my-5">
        <Formik
          initialValues={{
            name: "",
            age: "",
            mobile: "",
            email: "",
            gender: "",
            salaryRange: "",
            termPlan: "",
            plan: "",
            smokingHabbit: "",
            illness: "",
          }}
          validationSchema={Yup.object().shape({
            name: Yup.string().required("Required"),
            age: Yup.number().required("Required").min(18, "Can't be less than 18").max(60, "Can't be more than 99"),
            mobile: Yup.string()
              .required("Required")
              .matches(/^[0-9]{10,10}$/, "Invalid mobile number"),
            email: Yup.string().email("Invalid email id"),
            gender: Yup.string().required("Required"),
            salaryRange: Yup.string().required("Required"),
            plan: Yup.string().required("Required"),
            smokingHabbit: Yup.string().required("Required"),
            termPlan: Yup.string().required("Required"),
          })}
          onSubmit={(values) => {
            const incValue = insurance.annualPlan[values.gender]
            const normalTermValue = values.salaryRange / (values.termPlan === "Monthly" ? 1000 : 100)
            const price = Math.floor(normalTermValue * incValue)
            const idv = +values.plan * 100000
            let data = { ...insurance, userData: { ...values, price, idv } }
            dispatch({ type: ActionTypes.ADD_ITEM_TO_CART, payload: data })
            toast.success("Successfully added to your cart", {
              position: "top-right",
              autoClose: 3000,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
            })
          }}>
          <Form className="row justify-content-center">
            <div className="col-lg-6 col-md-9 col-sm-12 shadow-5 bg-light rounded-3 p-3">
              <h1 className="display-6 form-title">Fill Details</h1>
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Name</label>
                    <Field className="form-control" placeholder="Name" name="name" />
                    <ErrorMessage name="name" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Your Age</label>
                    <Field type="number" className="form-control" placeholder="Age (18 -99)" name="age" />
                    <ErrorMessage name="age" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Phone</label>
                    <div className="input-group">
                      <span className="input-group-text" id="addon-wrapping">
                        +91
                      </span>
                      <Field className="form-control" placeholder="XXXXXXXXXX" name="mobile" />
                    </div>
                    <ErrorMessage name="mobile" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Email</label>
                    <Field className="form-control" placeholder="asdfgh@example.com" name="email" />
                    <ErrorMessage name="email" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Gender</label>
                    <div>
                      <div className="form-check form-check-inline">
                        <Field className="form-check-input" type="radio" name="gender" value="male" />
                        <label className="form-check-label">Male</label>
                      </div>
                      <div className="form-check form-check-inline">
                        <Field className="form-check-input" type="radio" name="gender" value="female" />
                        <label className="form-check-label">Female</label>
                      </div>
                      <div className="form-check form-check-inline">
                        <Field className="form-check-input" type="radio" name="gender" value="other" />
                        <label className="form-check-label">Other</label>
                      </div>
                    </div>
                    <ErrorMessage name="gender" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label"> Salary Range </label>
                    <div className="position-relative">
                      <Field className="form-control" as="select" name="salaryRange">
                        <option value={""} disabled selected>
                          Select
                        </option>
                        <option value={SalRange[1].value}>{SalRange[1].name}</option>
                        <option value={SalRange[2].value}>{SalRange[2].name}</option>
                        <option value={SalRange[3].value}>{SalRange[3].name}</option>
                        <option value={SalRange[4].value}>{SalRange[4].name}</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                    <ErrorMessage name="salaryRange" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Smoking Habbit</label>
                    <div className="position-relative">
                      <Field className="form-control" as="select" name="smokingHabbit">
                        <option value={""} disabled>
                          Select
                        </option>
                        <option value={SmokingHabbits.NO_SMOKING}>{SmokingHabbits.NO_SMOKING}</option>
                        <option value={SmokingHabbits.OCASSIONAL}>{SmokingHabbits.OCASSIONAL}</option>
                        <option value={SmokingHabbits.REGULAR}>{SmokingHabbits.REGULAR}</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                    <ErrorMessage name="smokingHabbit" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Plan Duration</label>
                    <div className="position-relative">
                      <Field className="form-control" as="select" name="plan">
                        <option value={""} disabled>
                          Select
                        </option>
                        <option value={5}>{TermPlanYears[5]}</option>
                        <option value={25}>{TermPlanYears[25]}</option>
                        <option value={50}>{TermPlanYears[50]}</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                    <ErrorMessage name="plan" component="p" className="text-danger error-text" />
                  </div>
                </div>

                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Term Plan</label>
                    <div className="position-relative">
                      <Field className="form-control" as="select" name="termPlan">
                        <option value={""} disabled>
                          Select
                        </option>
                        <option value="Monthly">Monthly</option>
                        <option value="Yearly">Yearly</option>
                      </Field>
                      <span className="select-arrow"></span>
                    </div>
                    <ErrorMessage name="termPlan" component="p" className="text-danger error-text" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="my-2">
                    <label className="text-label">Current Illness</label>
                    <Field className="form-control" name="illness" placeholder="(Optional)" />
                    <ErrorMessage name="termPlan" component="p" className="text-danger error-text" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <Field>
                    {({ form: { values, errors } }) => {
                      if (!errors?.gender && values.gender?.length && values.termPlan?.length) {
                        const incValue = insurance.annualPlan[values.gender]
                        const normalTermValue = values.salaryRange / (values.termPlan === "Monthly" ? 1000 : 100)
                        const finalTermPlan = Math.floor(normalTermValue * incValue)
                        return (
                          <>
                            <h3 className="note note-primary">{"Term Plan: ₹ " + finalTermPlan ?? "- -"}</h3>
                          </>
                        )
                      }
                      return <></>
                    }}
                  </Field>
                  <Field>
                    {({ form: { values, errors } }) => {
                      if (!errors?.plan && values.plan && values.salaryRange) {
                        return (
                          <>
                            <h3 className="note note-secondary">
                              {"Insurred Value**: ₹ " +
                                (+values.plan === 5 ? "5 Lac" : +values.plan === 25 ? "25 Lac" : "50 Lac")}
                            </h3>
                            <small className="text-muted">
                              Value can differ based on smoking habbit and illness inspection if applicable.
                            </small>
                          </>
                        )
                      }
                      return <></>
                    }}
                  </Field>
                </div>
              </div>
              <button className="btn btn-primary btn-rounded btn-lg" type="submit">
                Add to Cart
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  )
}
