import { useEffect, useState } from "react"
import { Link, useHistory, useLocation, useParams } from "react-router-dom"
import { InsuranceHeader } from "../../components/InsuranceHeader/InsuranceHeader"
import { getInsuranceView } from "./Insurances"
import "./index.css"
import Header from "../../components/Header"
import { getIconFromType } from "../../utils/utils"
import InsuranceMetaData from "../../components/InsuranceMetaData"

const InsuranceInfo = () => {
  const history = useHistory()
  const location = useLocation()
  const { id } = useParams()
  const [insurance, setInsurance] = useState()

  useEffect(() => {
    if (location?.state?.insurance) {
      setInsurance(location.state.insurance)
    } else {
      console.log(id)
      history.push("/")
    }
  }, [])

  return (
    <>
      <Header />

      <main>
        <section className="container pt-5">
          <InsuranceHeader
            icon={getIconFromType(insurance?.type ?? "")}
            title={`${insurance?.type} Insurances`}
            links={[
              { name: "home", path: "/", isActive: false },
              {
                name: `${insurance?.type} insurances`,
                path: `/insurances/${insurance?.type.toLowerCase()}/1`,
                isActive: false,
              },
              { name: "insurance info", path: "#", isActive: true },
            ]}
          />
          {/* <InsuranceMetaData /> */}
          {insurance && getInsuranceView(insurance)}
        </section>
      </main>
    </>
  )
}

export default InsuranceInfo
