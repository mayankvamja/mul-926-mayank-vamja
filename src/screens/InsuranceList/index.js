import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, useParams } from "react-router-dom"
import { InsuranceHeader } from "../../components/InsuranceHeader/InsuranceHeader"
import { InsuranceCard } from "./InsuranceCard"
import SearchBox from "../../components/SearchBox"
import Pagination from "./Pagination"
import * as actions from "../../store/actions"
import "./index.css"
import Header from "../../components/Header"
import { getIconFromType } from "../../utils/utils"

const InsuranceList = () => {
  const { insurances, pageNo, totalPages, totalResults, pageSize } = useSelector((store) => store.insurance)
  const dispatch = useDispatch()
  const { type, page, keyword } = useParams()

  useEffect(() => dispatch(actions.setInsuranceList(type, page, pageSize, keyword)), [type, pageSize, keyword, page])

  return (
    <>
      <Header />
      <main>
        <section className="container pt-5">
          <SearchBox keyword={keyword} currentType={type.toUpperCase()} />
          <InsuranceHeader
            icon={getIconFromType(type)}
            title={`${type} Insurances`}
            links={[
              { name: "home", path: "/", isActive: false },
              { name: `${type} insurances`, path: `/insurances/${type}/1`, isActive: true },
            ]}
          />

          <div className="alert alert-light text-muted font-style-italiq">
            {keyword?.length > 0 && (
              <em className="me-4">
                Showing search results for : <b>{keyword}</b>
              </em>
            )}
            {totalResults > 0 && (
              <>
                (Showing : {insurances.length} out of {totalResults})
              </>
            )}
          </div>
          {insurances && insurances.length > 0 ? (
            <>
              <div className="row mt-4">
                {insurances?.map((item, index) => (
                  <Link
                    className="col col-md-6 col-sm-12 mb-3"
                    key={item._id}
                    to={{ pathname: "/insurance-info/" + item._id, state: { insurance: item } }}>
                    <InsuranceCard item={item} itemIndex={index} />
                  </Link>
                ))}
              </div>
              <Pagination
                page={pageNo}
                totalPages={totalPages}
                type={type}
                pageSize={pageSize}
                changePageSize={(size) => dispatch(actions.setInsuranceList(type, pageNo, size))}
              />
            </>
          ) : (
            <>
              <div className="col-md-9 col-sm-12">
                <div className="alert alert-primary m-5 border border-primary rounded-3">
                  <h4 className="alert-heading">No results found</h4>
                </div>
              </div>
            </>
          )}
        </section>
      </main>
    </>
  )
}

export default InsuranceList
