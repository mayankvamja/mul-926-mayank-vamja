import { getPriceTags, getRandomBgColor } from "../../utils/utils"

export const InsuranceCard = ({ item, itemIndex }) => (
  <div className="cust-card p-3 bg-white overflow-hidden">
    <div className="d-flex dlex-row align-items-center">
      <div className="avatar me-2">
        <img
          src={item.companyLogo}
          style={{ backgroundColor: getRandomBgColor(itemIndex) }}
          className="img-responsive"
          alt="bike"
        />
      </div>

      <div className="col">
        <h5 className="card-title">{item.companyName + " " + item.name}</h5>
        <p className="card-price mb-0">
          {getPriceTags(item.annualPlan, item.type, item.discount).map((tag, tagId) => (
            <span key={`price-tag-${item._id}-${tagId}`} className={`price-tag-${tagId} me-2`}>
              {tag}
            </span>
          ))}
        </p>
      </div>
    </div>
    <hr className="mb-2" />
    <div className="d-flex flex-row justify-content-between flex-wrap text-muted">
      <small>• No inspection required</small>
      <small>• Insurred instant</small>
    </div>
  </div>
)
