import { Link } from "react-router-dom"

const Pagination = ({ page, totalPages, type, pageSize, changePageSize }) => {
  return (
    <div className="pagination-wrapper my-3 d-flex flex-row align-items-center flex-wrap">
      <div class="btn-group bg-white rounded-pill m-2">
        <button class="btn btn-outline-primary btn-lg btn-rounded" type="button">
          Items per page
        </button>
        <button
          type="button"
          class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split btn-rounded"
          data-mdb-toggle="dropdown"
          aria-expanded="false">
          <span class="visually-hidden">Toggle Dropdown</span>
          <span>{pageSize}</span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <li>
            <button class="dropdown-item" onClick={() => changePageSize(10)}>
              10
            </button>
          </li>
          <li>
            <button class="dropdown-item" onClick={() => changePageSize(20)}>
              20
            </button>
          </li>
        </ul>
      </div>
      <nav>
        <ul className="pagination pagination-circle m-2">
          <li className={"me-2 page-item " + (+page === 1 ? "disabled" : "")}>
            <Link to={`/insurances/${type}/${page - 1}`} className="page-link">
              {"<"}
            </Link>
          </li>
          {[...Array(totalPages).keys()].map((i) => (
            <li className={"me-2 page-item " + (+page === i + 1 ? "disabled" : "")}>
              <Link
                to={page === i + 1 ? "#" : `/insurances/${type}/${i + 1}`}
                className={`page-link ${+page === i + 1 ? "bg-primary text-light active" : ""}`}>
                {i + 1}
              </Link>
            </li>
          ))}
          <li className={"me-2 page-item " + (+page === totalPages ? "disabled" : "")}>
            <Link to={+page === +totalPages ? "#" : `/insurances/${type}/${+page + 1}`} className="page-link">
              {">"}
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default Pagination
