import React, { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { Link, useHistory, useLocation } from "react-router-dom"
import { ActionTypes } from "../../store/actions/actionTypes"
import { CartItemBody } from "../Cart/CartItemBody"
import "./index.css"

const Checkout = () => {
  const dispatch = useDispatch()
  const location = useLocation()
  const history = useHistory()
  const [checkoutItems, setCheckoutItems] = useState([])
  const [code, setCode] = useState({ code: "", isApplied: false })
  const [gst, setGst] = useState(0)
  const [discount, setDiscount] = useState(0)
  const [finalPrice, setFinalPrice] = useState(0)
  const [totalPrice, setToatlPrice] = useState(0)
  const [agree, setAgreement] = useState(false)

  useEffect(() => {
    console.log(location.state)
    if (location?.state?.data?.length > 0) {
      setCheckoutItems(location.state.data)
      if (location.state?.code) {
        setCode(location.state.code)
        setDiscount(location.state.discount)
        setGst(location.state.gst)
        setFinalPrice(location.state.finalPrice)
        setToatlPrice(location.state.totalPrice)
      } else {
        setGst(location.state?.gst)
        setFinalPrice(location.state?.finalPrice)
        setToatlPrice(location.state?.totalPrice)
      }
    } else {
      history.push("/")
    }
  }, [location])

  const handleSubmit = () => {
    dispatch({ type: ActionTypes.DELETE_SELECTED })
    history.pop()
  }

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            PolicyMarket
          </Link>
        </div>
      </nav>

      <main>
        <section className="container pt-5">
          <h1 className="text-center display-3 mb-4">Checkout Your Order</h1>
          <div className="container checkout-list-wrapper">
            {checkoutItems &&
              checkoutItems.map((item) => (
                <div className="checkout-item-wrapper">
                  <div className="d-flex dlex-row align-items-center">
                    <div className="cart-item-avatar me-2">
                      <img src={item.companyLogo} className={"img-responsive "} alt="bike" />
                    </div>

                    <div className="col text-start">
                      <h3 className="card-title">{item.companyName + " " + item.name}</h3>
                    </div>
                  </div>
                  <CartItemBody item={item} />
                </div>
              ))}
            <div className="col-sm-12 mt-5 mx-auto summray-wrapper">
              {totalPrice > 0 && (
                <div className="card shadow-3 rounded-3 p-3 h-auto">
                  <h4 className="card-title text-center">Final Bill</h4>
                  <hr className="my-1 text-primary" />
                  <table className="table">
                    <tbody>
                      <tr className="text-muted font-weight-bold">
                        <td>Total Items</td>
                        <td>{checkoutItems.length}</td>
                      </tr>
                      <tr className="text-muted font-weight-bold">
                        <td>Amount</td>
                        <td>Rs {totalPrice}</td>
                      </tr>
                      <tr className="text-warning font-weight-bold">
                        <td>GST (5%)</td>
                        <td>Rs {gst}</td>
                      </tr>
                      {code?.isApplied && (
                        <>
                          <tr className="font-weight-bold">
                            <td>Coupen Code</td>
                            <td>{code?.code}</td>
                          </tr>
                          <tr className="font-weight-bold">
                            <td>Coupen Off</td>
                            <td>Rs {discount}</td>
                          </tr>
                        </>
                      )}

                      <tr className="text-success font-weight-bold">
                        <td>Toal Amount</td>
                        <td>Rs {finalPrice}</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox1"
                      value="agreement"
                      checked={agree}
                      onChange={(e) => setAgreement(e?.target?.checked)}
                    />
                    <label class="form-check-label" for="inlineCheckbox1">
                      <i>
                        I acknowledge that all above details are correct to my knowledge and I agree with all terms and
                        conditins of all policies.
                      </i>
                    </label>
                  </div>
                  <button disabled={!agree} onClick={handleSubmit} className="mt-3 btn btn-lg btn-primary btn-rounded">
                    Proceed To Pay
                  </button>
                </div>
              )}
            </div>
          </div>
        </section>
      </main>
    </>
  )
}

export default Checkout
