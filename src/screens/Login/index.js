import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { ActionTypes } from "../../store/actions/actionTypes"
import "./index.css"

const Login = () => {
  const { isLoggedIn } = useSelector((store) => store.auth)
  const dispatch = useDispatch()
  const history = useHistory()
  useEffect(() => (isLoggedIn ? history.push("/") : null), [history, isLoggedIn])
  const handleLogin = () => dispatch({ type: ActionTypes.LOGIN })

  return (
    <>
      <section className="d-flex justify-content-center align-items-center vh-100">
        <form style={{ width: "22rem" }} className="shadow p-4 rounded-3 bg-white">
          <h1 className="text-center text-primary text-3xl font-bold mb-2">PolicyMarket</h1>
          <h3 className="text-center text-muted text-decoration-underline font-bold mb-3">Login</h3>
          <div className="mb-3">
            <label className="form-label" htmlFor="form1Example1">
              Email address
            </label>
            <input required type="email" id="form1Example1" className="form-control" />
          </div>

          <div className="mb-3">
            <label className="form-label" htmlFor="form1Example2">
              Password
            </label>
            <input required type="password" id="form1Example2" className="form-control" />
          </div>

          <div className="row mb-4 d-none">
            <div className="col d-flex justify-content-center">
              <div className="form-check">
                <input className="form-check-input" type="checkbox" value="" id="rememberMe" defaultChecked={true} />
                <label className="form-check-label" htmlFor="rememberMe">
                  Remember me
                </label>
              </div>
            </div>

            <div className="col">
              <a href="#!">Forgot password?</a>
            </div>
          </div>

          <button type="submit" className="btn btn-primary btn-block btn-rounded" onClick={handleLogin}>
            Sign in
          </button>
        </form>
      </section>
    </>
  )
}

export default Login
