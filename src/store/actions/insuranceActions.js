import data, { bikeInsurances, carInsurances, propertyInsurances, termInsurances } from "../../utils/fakeData"
import { ActionTypes } from "./actionTypes"

const filterInsurances = (item, keyword) =>
  item.name.toLowerCase().includes(keyword.toLowerCase()) ||
  item.companyName.toLowerCase().includes(keyword.toLowerCase())

export const setInsuranceList = (type, pageNo, pageSize = 20, keyword = "") => (dispatch) => {
  let insurances = []
  let totalPages = 1
  switch (type) {
    case "car":
      insurances = carInsurances.filter((i) => filterInsurances(i, keyword))
      break

    case "bike":
      insurances = bikeInsurances.filter((i) => filterInsurances(i, keyword))
      break

    case "property":
      insurances = propertyInsurances.filter((i) => filterInsurances(i, keyword))
      break

    case "term":
      insurances = termInsurances.filter((i) => filterInsurances(i, keyword))
      break

    case "all":
      insurances = data.filter((i) => filterInsurances(i, keyword))
      break

    default:
      break
  }
  const totalResults = insurances.length
  totalPages = Math.ceil(insurances.length / pageSize)
  pageNo = +pageNo === 0 ? 1 : +pageNo > totalPages ? totalPages : pageNo
  insurances = insurances.slice((pageNo - 1) * pageSize, pageNo * pageSize)
  dispatch({
    type: ActionTypes.SET_INSURANCE_LIST,
    payload: { insurances, type, pageNo, totalResults, totalPages, pageSize },
  })
}

export const changePageSize = (newSize) => (dispatch) =>
  dispatch({ type: ActionTypes.CHANGE_PAGE_SIZE, payload: { pageSize: newSize } })
