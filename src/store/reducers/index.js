import cartReducer from './cartReducer'
import insuranceReducer from './insuranceReducer'
import authReducer from "./authReducer"
import { combineReducers } from "redux"

export default combineReducers({
  cart: cartReducer,
  insurance: insuranceReducer,
  auth: authReducer,
})
