import { ActionTypes } from "../actions/actionTypes"

const initialState = {
  cartItems: [],
  selectedItems: 0,
  totalPrice: 0,
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.ADD_ITEM_TO_CART:
      return {
        ...state,
        totalPrice: state.totalPrice + payload?.userData?.price,
        selectedItems: state.selectedItems + 1,
        cartItems: [...state.cartItems, { ...payload, selected: true, dateAdded: Date.parse(new Date()) }],
      }

    case ActionTypes.DELETE_ITEM_FROM_CART:
      let oldItems = [...state.cartItems]
      oldItems.splice(payload, 1)
      console.log("PAYLOAD : ", payload)
      console.log("OLDITems : ", oldItems)
      return {
        ...state,
        totalPrice: state.totalPrice - state.cartItems[payload]?.userData?.price,
        selectedItems: state.cartItems[payload].selected ? state.selectedItems - 1 : state.selectedItems,
        cartItems: [...oldItems],
      }

    case ActionTypes.DELETE_SELECTED:
      return {
        ...state,
        totalPrice: 0,
        selectedItems: 0,
        cartItems: state.cartItems.filter((i) => !i.selected),
      }

    case ActionTypes.CHANGE_CART_SELECTION:
      let newSelectedItems = state.cartItems[payload].selected ? state.selectedItems - 1 : state.selectedItems + 1
      let newTodalPrice = state.cartItems[payload].selected
        ? state.totalPrice - state.cartItems[payload].userData.price
        : state.totalPrice + state.cartItems[payload].userData.price
      let items = [...state.cartItems]
      items[payload].selected = !items[payload].selected
      return {
        ...state,
        totalPrice: newTodalPrice,
        selectedItems: newSelectedItems,
        cartItems: [...items],
      }

    case ActionTypes.CART_SELECT_ALL:
      console.log(state.cartItems.reduce((a, b) => a + b.userData.price, 0))
      return {
        ...state,
        selectedItems: state.cartItems.length,
        totalPrice: state.cartItems.reduce((a, b) => a + b.userData.price, 0),
        cartItems: [...state.cartItems.map((i) => ({ ...i, selected: true }))],
      }

    case ActionTypes.CART_DESELECT_ALL:
      return {
        ...state,
        selectedItems: 0,
        totalPrice: 0,
        cartItems: [...state.cartItems.map((i) => ({ ...i, selected: false }))],
      }

    default:
      return { ...state }
  }
}

export default reducer
