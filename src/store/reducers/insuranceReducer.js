import { ActionTypes } from "../actions/actionTypes"

const initialState = {
  insurances: [],
  pageNo: 1,
  pageSize: 20,
  totalPages: 1,
  type: "",
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_INSURANCE_LIST:
      return { ...state, ...payload }

    case ActionTypes.CHANGE_PAGE_SIZE:
      return { ...state, ...payload }

    default:
      return { ...state }
  }
}

export default reducer
