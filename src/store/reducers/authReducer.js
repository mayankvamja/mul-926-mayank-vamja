import { ActionTypes } from "../actions/actionTypes"

const initialState = {
  isLoggedIn: localStorage.getItem("isLoggedIn") ? localStorage.getItem("isLoggedIn") : false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.LOGIN:
      localStorage.setItem("isLoggedIn", true)
      return { ...state, isLoggedIn: true }

    case ActionTypes.LOGOUT:
      localStorage.removeItem("isLoggedIn")
      return { ...state, isLoggedIn: false }

    default:
      return { ...state }
  }
}

export default reducer
