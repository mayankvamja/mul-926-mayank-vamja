export const SalRange = {
  1: { name: "2 -5 Lacs", value: 300000 },
  2: { name: "5 -12 Lacs", value: 750000 },
  3: { name: "12 -25 Lacs", value: 1500000 },
  4: { name: "More than 25 Lacs", value: 2500000 },
}

export const SmokingHabbits = {
  NO_SMOKING: "No Smoking",
  OCASSIONAL: "Ocassional Smoking",
  REGULAR: "Regular Smoking",
}

export const TermPlanYears = {
  5: "5 years",
  25: "25 years",
  50: "Till 60 years",
}
