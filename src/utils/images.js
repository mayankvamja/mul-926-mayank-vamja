import IC_CAR from "../assets/images/ic_car.svg"
import IC_BIKE from "../assets/images/ic_bike.svg"
import IC_PROPERTY from "../assets/images/ic_property.svg"
import IC_TERM from "../assets/images/ic_term.svg"
import IC_LIST from "../assets/images/ic_list.svg"

export { IC_CAR, IC_BIKE, IC_PROPERTY, IC_TERM, IC_LIST }
