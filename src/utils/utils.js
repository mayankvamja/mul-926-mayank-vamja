import { IC_BIKE, IC_CAR, IC_LIST, IC_PROPERTY, IC_TERM } from "./images"

export const bgColors = ["#1266F130", "#B23CFD30", "#00B74A30", "#F9315430", "#FFA90030"]

export const getRandomBgColor = (key) => bgColors[key % 5]

export const getPriceTags = (val, type, discount) =>
  type === "TERM"
    ? [`Know more >`, "", `${discount}% off`]
    : type === "PROPERTY"
    ? [
        `Starting from ₹${Math.floor(val?.structure - val?.structure * (discount / 100))}`,
        `(₹${val?.structure})`,
        `-${discount}%`,
      ]
    : [`₹${Math.floor(val - val * (discount / 100))}`, `(₹${val})`, `-${discount}%`]

export const getIconFromType = (type) => {
  switch (type.toUpperCase()) {
    case "CAR":
      return IC_CAR
    case "BIKE":
      return IC_BIKE
    case "PROPERTY":
      return IC_PROPERTY
    case "TERM":
      return IC_TERM
    default:
      return IC_LIST
  }
}
