import { useSelector } from "react-redux"
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom"
import Cart from "../../screens/Cart"
import Checkout from "../../screens/Checkout"
import Home from "../../screens/Home"
import InsuranceInfo from "../../screens/InsuranceInfo"
import InsuranceList from "../../screens/InsuranceList"
import Login from "../../screens/Login"

const AppRouter = () => {
  const { isLoggedIn } = useSelector((store) => store.auth)

  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        {isLoggedIn ? (
          <>
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/checkout" component={Checkout} />
            <Route exact path="/insurances/:type/:keyword/:page" component={InsuranceList} />
            <Route exact path="/insurances/:type/:page" component={InsuranceList} />
            <Route exact path="/insurance-info/:id" component={InsuranceInfo} />
          </>
        ) : (
          <Redirect to="/login" />
        )}
      </Switch>
    </Router>
  )
}

export default AppRouter
