import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { ActionTypes } from "../../store/actions/actionTypes"
import "./index.css"

const Header = () => {
  const totalCartItems = useSelector((store) => store.cart?.cartItems?.length ?? 0)
  const dispatch = useDispatch()

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          PolicyMarket
        </Link>
        <div>
          <Link to="/cart" className="cart-btn btn btn-outline-primary btn-rounded">
            <i className="fas fa-shopping-cart fa-lg"></i> CART
            {totalCartItems > 0 && (
              <span className="ms-2 badge rounded-pill badge-notification bg-danger">{totalCartItems}</span>
            )}
          </Link>
          <button
            onClick={() => dispatch({ type: ActionTypes.LOGOUT })}
            className="btn btn-light text-primary ms-3 btn-rounded">
            Logout
          </button>
        </div>
      </div>
    </nav>
  )
}

export default Header
