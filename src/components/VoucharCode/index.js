import { toast } from "react-toastify"
import "./index.css"

const VoucharCode = ({ code, setCode }) => {
  return (
    <div className="input-group my-2">
      <input
        type="text"
        placeholder="APPLY COUPEN"
        className="form-control"
        value={code.code}
        onChange={(e) => setCode({ code: e?.target?.value?.toUpperCase(), isApplied: false })}
        disabled={code.isApplied}
      />
      {!code.isApplied ? (
        <button
          type="button"
          className="btn btn-primary"
          onClick={() =>
            code?.code === "FIVEOFF"
              ? setCode({ code: code?.code, isApplied: true })
              : toast.error("Invalid Coupen Code", {
                  position: "top-right",
                  autoClose: 3000,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                })
          }>
          <i className="fa fa-chevron-circle-right"></i>
        </button>
      ) : (
        <button type="button" className="btn btn-success" onClick={() => setCode({ code: "", isApplied: false })}>
          <i className="fa fa-trash"></i>
        </button>
      )}
    </div>
  )
}

export default VoucharCode
