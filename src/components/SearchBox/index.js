import { Field, Form, Formik } from "formik"
import { useEffect, useState } from "react"
import { useHistory } from "react-router-dom"
import "./index.css"

const SearchBox = ({ keyword = "", currentType = "ALL" }) => {
  const history = useHistory()
  const [search, setSearch] = useState(keyword ?? "")
  const [searchType, setSearchType] = useState(currentType)

  useEffect(() => searchType !== currentType && submit(), [searchType])

  const submit = () =>
    history.push(`/insurances/${searchType.toLowerCase()}/${search?.length > 0 ? search + "/1" : "1"}`)

  const handleSearchChange = (e) => setSearch(e.target?.value)

  const handleSearchTypeChange = (e) => setSearchType(e.target?.value)

  return (
    <>
      <form onSubmit={() => submit()}>
        <div className="col-lg-6 col-sm-12 mb-5 search-bar-wrapper mx-auto">
          <div className="d-flex input-group w-auto rounded-full mb-2">
            <input
              onChange={handleSearchChange}
              value={search}
              autoComplete="off"
              id="search"
              name="search"
              className="form-control"
              placeholder="Search"
            />

            <button className="btn btn-primary btn-rounded" type="submit" data-mdb-ripple-color="dark">
              <i className="fa fa-search"></i>
            </button>
          </div>
          <div className="options-selection  text-center">
            <div className="form-check form-check-inline">
              <input
                onChange={handleSearchTypeChange}
                checked={searchType === "BIKE"}
                className="form-check-input"
                type="radio"
                id="inlineRadio1"
                value="BIKE"
              />
              <label className="form-check-label" htmlFor="inlineRadio1">
                Bike
              </label>
            </div>

            <div className="form-check form-check-inline">
              <input
                onChange={handleSearchTypeChange}
                checked={searchType === "CAR"}
                className="form-check-input"
                type="radio"
                id="inlineRadio2"
                value="CAR"
              />
              <label className="form-check-label" htmlFor="inlineRadio2">
                Car
              </label>
            </div>

            <div className="form-check form-check-inline">
              <input
                onChange={handleSearchTypeChange}
                checked={searchType === "PROPERTY"}
                className="form-check-input"
                type="radio"
                id="inlineRadio3"
                value="PROPERTY"
              />
              <label className="form-check-label" htmlFor="inlineRadio3">
                Property
              </label>
            </div>

            <div className="form-check form-check-inline">
              <input
                onChange={handleSearchTypeChange}
                checked={searchType === "TERM"}
                className="form-check-input"
                type="radio"
                id="inlineRadio4"
                value="TERM"
              />
              <label className="form-check-label" htmlFor="inlineRadio4">
                Term Plan
              </label>
            </div>

            <div className="form-check form-check-inline">
              <input
                onChange={handleSearchTypeChange}
                checked={searchType === "ALL"}
                className="form-check-input"
                type="radio"
                id="inlineRadio5"
                value="ALL"
              />
              <label className="form-check-label" htmlFor="inlineRadio5">
                All
              </label>
            </div>
          </div>
        </div>
        <hr />
      </form>
    </>
  )
}

export default SearchBox
