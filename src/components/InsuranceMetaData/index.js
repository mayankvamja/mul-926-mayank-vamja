import React from "react"
import "./index.css"

const InsuranceMetaData = () => {
  return (
    <div className="meta-data-wrapper m-3 p-4">
      <div className="row">
        <div className="col-md-6 col-sm-12">
          <h4>What is covered?</h4>
          <ul className="text-muted">
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
            <li>Vestibulum auctor dapibus neque.</li>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
            <li>Vestibulum auctor dapibus neque.</li>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
            <li>Vestibulum auctor dapibus neque.</li>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
            <li>Vestibulum auctor dapibus neque.</li>
          </ul>
        </div>
        <div className="col-md-6 col-sm-12">
          <h4>What is not covered?</h4>
          <ul className="text-muted">
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Vestibulum auctor dapibus neque.</li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default InsuranceMetaData
