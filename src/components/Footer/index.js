const Footer = () => {
  return (
    <div class="text-center p-3 mt-5" style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}>
      © 2020
      <span class="text-dark">PolicyMarket</span>
    </div>
  )
}

export default Footer
