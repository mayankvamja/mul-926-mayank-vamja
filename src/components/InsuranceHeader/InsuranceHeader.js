import { Link } from "react-router-dom"
import IC_LIST from "../../assets/images/ic_list.svg"
import "./index.css"

export const InsuranceHeader = ({
  icon = IC_LIST,
  title = "Insurances",
  subtitle = "Find Your best match",
  links = null,
}) => (
  <>
    <div className="d-flex flex-row mb-4">
      <div className="me-2 rounded-circle overflow-hidden title-icon">
        <img className="p-3 img-responsive" src={icon} alt="i-group" />
      </div>
      <div className="col">
        <h2 className="m-0 title-text">{title}</h2>
        <small>
          <i className="text-muted">{subtitle}</i>
        </small>
      </div>
    </div>
    {links && (
      <div className="my-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            {links.map((link) => (
              <li class="breadcrumb-item" key={"breadcrumb-link-" + link.name}>
                {link.isActive ? link.name : <Link to={link.path}>{link.name}</Link>}
                <i className="text-muted fa fa-chevron-right ms-2"></i>
              </li>
            ))}
          </ol>
        </nav>
      </div>
    )}
  </>
)
