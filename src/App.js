import { Provider } from "react-redux"
import AppRouter from "./components/AppRouter"
import Footer from "./components/Footer"
import store from "./store"

const App = () => (
  <Provider store={store}>
    <AppRouter />
    <Footer />
  </Provider>
)

export default App
